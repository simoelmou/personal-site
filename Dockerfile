FROM golang:1.13.4-alpine as builder
RUN mkdir /build
ADD . /build/
WORKDIR /build
RUN go build -o site .
FROM alpine
RUN adduser -S -D -H -h /app appuser
USER appuser
COPY --from=builder /build/site /app/
USER root
ADD templates /app/templates
RUN ls -all / && chmod -R 777 /app/
USER appuser
WORKDIR /app
CMD ["./site"]
