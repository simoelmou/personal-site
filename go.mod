module PersonalSite

go 1.13

require (
	github.com/gin-gonic/gin v1.6.1
	github.com/google/go-github/v30 v30.1.0
	github.com/xanzy/go-gitlab v0.29.0
	golang.org/x/oauth2 v0.0.0-20181106182150-f42d05182288
)
