package internal

import (
	"context"
	"encoding/json"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
	"io/ioutil"
	"log"
	"net/http"
)

// Endpoint is Bitbucket's OAuth 2.0 endpoint.
var endpoint = oauth2.Endpoint{
	TokenURL: "https://bitbucket.org/site/oauth2/access_token",
}

func GetBitbucketProjects(clientId, clientSecret string) []Project {
	token, err := NewOAuthClientCredentials(clientId, clientSecret)
	if err != nil {
		log.Fatal(err)
	}
	url := "https://bitbucket.org/!api/internal/dashboard/repositories?q=is_private=false"
	method := "GET"

	client := &http.Client{
	}
	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Add("Authorization", "Bearer "+token)

	res, err := client.Do(req)
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	bitProjects, err := unmarshalBitbucketProjects(body)
	if err != nil {
		log.Fatal(err)
	}
	var projects []Project
	for _, repository := range bitProjects.Projects {
		projects = append(projects, Project{
			Site:        "Bitbucket",
			Lang:        *repository.Language,
			Name:        *repository.Name,
			Description: *repository.Description,
			Link:        *repository.Links.HTML.Href,
			Stars:       0,
			Forks:       0,
			Members:     1,
		})
	}
	return projects
}

// Uses the Client Credentials Grant oauth2 flow to authenticate to Bitbucket
func NewOAuthClientCredentials(i, s string) (string, error) {
	ctx := context.Background()
	conf := &clientcredentials.Config{
		ClientID:     i,
		ClientSecret: s,
		TokenURL:     endpoint.TokenURL,
	}

	tok, err := conf.Token(ctx)
	if err != nil {
		return "", err
	}
	return tok.AccessToken, nil

}

func unmarshalBitbucketProjects(data []byte) (BitbucketProjects, error) {
	var r BitbucketProjects
	err := json.Unmarshal(data, &r)
	return r, err
}

type BitbucketProjects struct {
	Pagelen  *int64             `json:"pagelen,omitempty"`
	Projects []BitbucketProject `json:"values"`
	Page     *int64             `json:"page,omitempty"`
	Size     *int64             `json:"size,omitempty"`
}

type BitbucketProject struct {
	SCM         *string     `json:"scm,omitempty"`
	Website     *string     `json:"website"`
	HasWiki     *bool       `json:"has_wiki,omitempty"`
	Name        *string     `json:"name,omitempty"`
	Links       *ValueLinks `json:"links,omitempty"`
	UUID        *string     `json:"uuid,omitempty"`
	Language    *string     `json:"language,omitempty"`
	CreatedOn   *string     `json:"created_on,omitempty"`
	FullName    *string     `json:"full_name,omitempty"`
	HasIssues   *bool       `json:"has_issues,omitempty"`
	Owner       *Owner      `json:"owner,omitempty"`
	UpdatedOn   *string     `json:"updated_on,omitempty"`
	Size        *int64      `json:"size,omitempty"`
	Type        *string     `json:"type,omitempty"`
	Slug        *string     `json:"slug,omitempty"`
	IsPrivate   *bool       `json:"is_private,omitempty"`
	Description *string     `json:"description,omitempty"`
}

type ValueLinks struct {
	Watchers     *Avatar `json:"watchers,omitempty"`
	Branches     *Avatar `json:"branches,omitempty"`
	Tags         *Avatar `json:"tags,omitempty"`
	Commits      *Avatar `json:"commits,omitempty"`
	Clone        []Clone `json:"clone"`
	Self         *Avatar `json:"self,omitempty"`
	Source       *Avatar `json:"source,omitempty"`
	HTML         *Avatar `json:"html,omitempty"`
	Avatar       *Avatar `json:"avatar,omitempty"`
	Hooks        *Avatar `json:"hooks,omitempty"`
	Forks        *Avatar `json:"forks,omitempty"`
	Downloads    *Avatar `json:"downloads,omitempty"`
	Pullrequests *Avatar `json:"pullrequests,omitempty"`
}

type Avatar struct {
	Href *string `json:"href,omitempty"`
}

type Clone struct {
	Href *string `json:"href,omitempty"`
	Name *string `json:"name,omitempty"`
}

type Owner struct {
	DisplayName *string     `json:"display_name,omitempty"`
	UUID        *string     `json:"uuid,omitempty"`
	Links       *OwnerLinks `json:"links,omitempty"`
	Nickname    *string     `json:"nickname,omitempty"`
	Type        *string     `json:"type,omitempty"`
	AccountID   *string     `json:"account_id,omitempty"`
	Username    *string     `json:"username,omitempty"`
}

type OwnerLinks struct {
	Self   *Avatar `json:"self,omitempty"`
	HTML   *Avatar `json:"html,omitempty"`
	Avatar *Avatar `json:"avatar,omitempty"`
}
