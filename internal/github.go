package internal

import (
	"context"
	"github.com/google/go-github/v30/github"
	"golang.org/x/oauth2"
)

func GetGithubProjects(username, token string) []Project {
	ctx := context.Background()
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: token},
	)
	tc := oauth2.NewClient(ctx, ts)

	client := github.NewClient(tc)
	opt := &github.RepositoryListOptions{Visibility: "public", Sort: "created"}
	gitlabProjects, _, err := client.Repositories.List(ctx, username, opt)
	if err != nil {
		panic(err)
	}
	var projects []Project
	for _, project := range gitlabProjects {
		members, _, err := client.Repositories.ListContributors(ctx, username, *project.Name, nil)
		if err != nil {
			panic(err)
		}
		projects = append(projects, Project{
			Site:        "Github",
			Lang:        project.GetLanguage(),
			Name:        project.GetName(),
			Description: project.GetDescription(),
			Link:        project.GetHTMLURL(),
			Stars:       project.GetStargazersCount(),
			Forks:       project.GetForksCount(),
			Members:     len(members),
		})
	}
	return projects
}
