package internal

import (
	"github.com/xanzy/go-gitlab"
)

func GetGitlabProjects(token string) []Project {
	git := gitlab.NewClient(nil, token)
	owned := true
	visibility := gitlab.PublicVisibility
	opt := &gitlab.ListProjectsOptions{Owned: &owned, Visibility: &visibility}
	gitlabProjects, _, err := git.Projects.ListProjects(opt)
	if err != nil {
		panic(err)
	}
	var projects []Project
	for _, project := range gitlabProjects {
		members, _, err := git.ProjectMembers.ListAllProjectMembers(project.ID, &gitlab.ListProjectMembersOptions{})
		lang, _, err := git.Projects.GetProjectLanguages(project.ID)
		if err != nil {
			panic(err)
		}
		projects = append(projects, Project{
			Site:        "Gitlab",
			Lang:        getUsedLanguage(lang),
			Name:        project.Name,
			Description: project.Description,
			Link:        project.WebURL,
			Stars:       project.StarCount,
			Forks:       project.ForksCount,
			Members:     len(members),
		})
	}
	return projects
}

func getUsedLanguage(langs *gitlab.ProjectLanguages) string {
	usedLang := ""
	max := float32(0)
	for lang, percentage := range *langs {
		if percentage > max {
			max = percentage
			usedLang = lang
		}
	}
	return usedLang
}
