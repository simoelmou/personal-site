package internal

type Resume struct {
	Projects []Project
}

type Project struct {
	Site        string
	Lang        string
	Name        string
	Description string
	Link        string
	Stars       int
	Forks       int
	Members     int
}
