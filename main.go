package main

import (
	"PersonalSite/internal"
	"github.com/gin-gonic/gin"
	"html/template"
	"log"
	"net/http"
	"os"
	"time"
	"unicode"
)

func main() {
	router := gin.Default()
	funcMap := template.FuncMap{
		"ToCamel": ToCamel,
	}
	router.SetFuncMap(funcMap)
	router.LoadHTMLGlob("templates/*.tmpl")
	router.Static("/style", "./templates/style")
	router.Static("/assets", "./templates/assets")
	router.Static("/js", "./templates/js")

	resume := internal.Resume{}
	gitlabToken := os.Getenv("GITLAB_TOKEN")
	bitbucketClientId := os.Getenv("BITBUCKET_CLIENTID")
	bitbucketClientSecret := os.Getenv("BITBUCKET_CLIENTSECRET")
	githubUsername := os.Getenv("GITHUB_USERNAME")
	githubToken := os.Getenv("GITHUB_TOKEN")

	var err error
	updateDuration := 24 * time.Hour
	if os.Getenv("UPDATE_DURATION") != "" {
		updateDuration, err = time.ParseDuration(os.Getenv("UPDATE_DURATION"))
		if err != nil {
			panic(err)
		}
	}
	resume.Projects = getProjects(gitlabToken, bitbucketClientId, bitbucketClientSecret, githubUsername, githubToken)

	go func() {
		ticker := time.NewTicker(updateDuration)
		for {
			select {
			case <-ticker.C:
				log.Println("Update Projects")
				resume.Projects = getProjects(gitlabToken, bitbucketClientId, bitbucketClientSecret, githubUsername, githubToken)
			}
		}
	}()

	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl", resume)
	})
	if err := router.Run(":8080"); err != nil {
		panic(err)
	}
}

func getProjects(gitlabToken, bitbucketClientId, bitbucketClientSecret, githubUsername, githubToken string) []internal.Project {
	var projects []internal.Project
	projects = append(projects, internal.GetGitlabProjects(gitlabToken)...)
	projects = append(projects, internal.GetBitbucketProjects(bitbucketClientId, bitbucketClientSecret)...)
	projects = append(projects, internal.GetGithubProjects(githubUsername, githubToken)...)
	return projects
}

func ToCamel(lang string) string {
	text := []byte(lang)
	if lang != "" {
		text[0] = byte(unicode.ToUpper(rune(lang[0])))
	}
	return string(text)
}
